import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  primerNumero:number;
  segundoNumero:number;
  resultado:number;

  @Output() operacionCreada = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onSumar(){
    let resultado = this.primerNumero + this.segundoNumero;
    this.operacionCreada.emit(resultado);
  }

  onRestar(){
    let resultado = this.primerNumero - this.segundoNumero;
    this.operacionCreada.emit(resultado);
  }

}
